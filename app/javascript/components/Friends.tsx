import * as React from "react";

interface IFriend {
  name: string;
}

interface P {}
interface S {
  loading: boolean;
  friends: IFriend[];
}

class Friends extends React.Component<P, S> {
  constructor(props: P) {
    super(props);
    this.state = {
      loading: true,
      friends: []
    }
    this.getFriends();
  }

  private getFriends() {
    fetch("friends").then(response => {
      if (response.ok) {
        const friends = response.blob();
        console.log(friends);
        this.setState(() => ({ friends, loading: false }));
      } else {
        throw Error("Could not retrieve Friends :(");
      }
    });
  }

  render () {
    return (
      <section>
        { this.state.loading ? <p>Getting friends..</p> : <p>I have { this.state.friends.length }</p> }
      </section>
    );
  }
}

export default Friends;
