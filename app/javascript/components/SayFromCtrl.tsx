import * as React from "react";

interface P {
  message: string;
}
interface S {}

class SayFromCtrl extends React.Component<P, S> {
  render () {
    return (
      <div>
        <div>Message: {this.props.message}</div>
      </div>
    );
  }
}

export default SayFromCtrl;
