import * as React from "react";

interface P {
  greeting: string;
}
interface S {}

class HelloWorld extends React.Component<P, S> {
  constructor(props: P) {
    super(props);
  }

  render () {
    return (
      <div>
        <div>Greeting: {this.props.greeting}</div>
      </div>
    );
  }
}

export default HelloWorld;
