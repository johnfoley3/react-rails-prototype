# The greatest welcome greeter in the world
class WelcomeController < ApplicationController
  def index
    @message = Time.now.to_s
  end
end
