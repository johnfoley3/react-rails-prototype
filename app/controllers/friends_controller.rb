class FriendsController < ApplicationController
  def index
    respond_to do |format|
      format.json { render json: Friend.all }
      format.html do
        @friends = Friend.all
      end
    end
  end

  def show
  end

  def create
  end

  def destroy
  end
end
