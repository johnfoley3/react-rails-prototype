.PHONY: dev

all: dev

install:
	bundle install && yarn install

webpack-dev-server:
	./bin/webpack-dev-server

rails-dev-server:
	bundle exec rails s

dev:
	make -j4 webpack-dev-server rails-dev-server
